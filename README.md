Zim Trello Plugin
=======================

This is a plugin for [Zim Desktop Wiki](http://www.zim-wiki.org/) that provides basic
integration with Trello. THIS PLUGIN IS NO LONGER MAINTAINED.

You can use it to create Zim pages corresponding to Trello cards, for example
to store your personal notes or long documentation not suitable for the Trello
UI experience.

You will need a Trello API token to use this plugin. To generate this token, visit [the Trello token generation page](https://trello.com/1/authorize?key=67180398ba2e48b8dc0e77d9ddf41af2&name=Zim%20Trello%20Plugin&expiration=never&response_type=token&scope=read) and then copy the generated token into this plugin's preferences.

The Trello ID is stored in the page headers, so you can see/modify it if you
go to Tools > Edit Source.

To install this plugin, clone the repo directly into your ~/.local/share/zim/plugins/ folder. It will then show up in your Edit > Preferences > Plugins list.
