#!/usr/bin/python

import gtk
import json

import re
import urllib
import logging

from zim.signals import ConnectorMixin
from zim.actions import action
from zim.formats import ParseTree

from zim.plugins import PluginClass,extends,WindowExtension
from zim.gui.widgets import Dialog,TOP_PANE,PANE_POSITIONS

# Trello API key for my trello stuffs
API_KEY="67180398ba2e48b8dc0e77d9ddf41af2"

# Main plugin class that zim loads in its plugins page
class TrelloPlugin(PluginClass):
	plugin_info={
		'name'       :_('Trello'),  # T: plugin name
		'description':_("""This plugin provides basic Trello integration.

You can use it to create Zim pages corresponding to Trello cards, for example
to store your personal notes or long documentation not suitable for the Trello
UI experience.

You will need a Trello API token to use this plugin. To generate this token, visit

https://trello.com/1/authorize?key="""+API_KEY+"""&name=Zim Trello Plugin&expiration=never&response_type=token&scope=read

and then copy the generated token into this plugin's preferences.

The Trello ID is stored in the page headers, so you can see/modify it if you
go to Tools > Edit Source.
							"""),
		# T: plugin description
		'author'     :'DeinonychusCowboy <blackline.silverfield@gmail.com>',
	}

	plugin_preferences=(
		# key, type, label, default
		('pane','choice',_('Position in the window'),TOP_PANE,PANE_POSITIONS),
		('token','string',_('Trello API Token'),""),

		('assigned','bool',_('Only show cards you are a member of'),False),
		('board','string',_('Limit to cards from the board named'),""),
		('list','string',_('Limit to cards from the list(s) named'),""),

		('replacement','choice',_('Replace filesystem unsafe characters in card title with'),"Nothing",["Nothing","Underscore"]),
	)

	def __init__(self,config=None):
		PluginClass.__init__(self,config)
		self.preferences.connect('changed',self.on_preferences_changed)
		self.on_preferences_changed(self.preferences)

	def on_preferences_changed(self,preferences):
		self.set_extension_class('MainWindow',MainWindowExtension)
		# update token
		TrelloApi.setToken(preferences["token"])

# API interface, meant to be used statically
class TrelloApi:
	_token=None
	@staticmethod
	def setToken(token):
		TrelloApi._token=token
	@staticmethod
	def get(path):
		if TrelloApi._token is None:
			logging.getLogger("zim").warn("No Trello API token, query aborted")
			return {}

		url="https://api.trello.com/1/"+path+"?key=" + API_KEY + "&token=" + TrelloApi._token
		logging.getLogger("zim").debug("Querying "+url)
		response = urllib.urlopen(url).read()
		return json.loads(response)

# extension that adds toolbar button and pane
@extends('MainWindow',autoload=False)
class MainWindowExtension(WindowExtension):
	_location=None
	_preferences=None
	_fssafe=None
	uimanager_xml = '''
		<ui>
		<menubar name='menubar'>
			<menu action='insert_menu'>
				<placeholder name='plugin_items'>
					<menuitem action='open_card'/>
				</placeholder>
			</menu>
		</menubar>
		<toolbar name='toolbar'>
				<placeholder name='format'>
					<toolitem action='open_card'/>
				</placeholder>
			</toolbar>
		</ui>
	'''
	def __init__(self,plugin,window):
		WindowExtension.__init__(self,plugin,window)
		self.widget=SidePaneTrello(self.window.ui,self.window.pageview,plugin)
		self.on_preferences_changed(plugin.preferences)
		self.connectto(plugin.preferences,'changed',self.on_preferences_changed)
		self._fssafe=re.compile(r"[ \n\\/:*?""<>|]")

	def on_preferences_changed(self,preferences):
		self._preferences=preferences
		if self.widget is None:
			return

		if preferences['pane'] != self._location:
			self._location=preferences['pane']
			try:
				self.window.remove(self.widget)
			except ValueError:
				pass
			self.window.add_tab(
				_('Trello'),self.widget,preferences['pane'])

		# T: widget label
		self.widget.show_all()

	def teardown(self):
		self.window.remove(self.widget)
		self.widget.disconnect_all()
		self.widget=None

	# TODO this should use the trello logo
	@action(_('Open Trello Card'), stock='gtk-dnd-multiple', readonly=False)
	def open_card(self):
		cardid = OpenTrelloCardDialog(self.window, self._preferences).run()
		if not cardid:
			return

		# create new page for card
		card_info=TrelloApi.get("cards/" + cardid)

		page=self.window.ui.notebook.get_new_page(
			self.window.ui.notebook.pages.lookup_from_user_input(
				"+" + self._fssafe.sub( ("_","")[self._preferences["replacement"]=="Nothing"] ,card_info["name"]),
				self.window.pageview.get_page()
			)
		)
		parsetree=ParseTree()
		parsetree.fromstring("<zim-tree></zim-tree>")
		parsetree.set_heading(card_info["name"])
		parsetree.meta["X-Trello-ID"]=cardid
		page.set_parsetree(parsetree)

		self.window.ui.open_page(page)

# Trello pane
class TrelloWidget(ConnectorMixin, gtk.ScrolledWindow):
	_id=None
	_ui=None
	_tid=None
	_pane=None
	_pageview=None

	def __init__(self,ui,pageview,plugin):
		gtk.ScrolledWindow.__init__(self)
		self._id=re.compile(r".*\[Trello:([^\]]*)\].*")
		# TODO do we actually want this
		self._pane=TrelloView()
		self.add_with_viewport(self._pane)
		self.connectto(plugin.preferences,'changed',self.on_preferences_changed)

		# XXX remove ui - use signals from pageview for this
		self._ui=ui
		self._pageview=pageview
		self.connectto(ui,'open-page',self.on_open_page)

		self.on_preferences_changed(plugin.preferences)

	def on_preferences_changed(self,preferences):
		self.check(self._pageview.get_page())

	def on_open_page(self,ui,page,path):
		self.check(page)

	def check(self,page):
		if page is not None and page.get_parsetree() is not None and page.get_parsetree().meta is not None and "X-Trello-ID" in page.get_parsetree().meta:
			card_info=TrelloApi.get("cards/" + page.get_parsetree().meta["X-Trello-ID"])
			self._pane.set_content(card_info)
		else:
			self._pane.reset_content()

# contents of Trello pane
class TrelloView(gtk.VBox):
	_textview=None
	_title=None

	def __init__(self, *args, **kwargs):
		super(TrelloView, self).__init__()

		self._textview=gtk.TextView()
		self._textview.set_editable(False)
		self._title=gtk.Label()

		self.add(self._title)
		self.add(self._textview)

	def set_content(self,trello_info):
		self._title.set_text(trello_info["name"])
		#TODO labels
		#TODO assignees
		#TODO dates
		self._textview.get_buffer().set_text(trello_info["desc"])
	def reset_content(self):
		self.set_content({"name":"","desc":""})

# sidepane version of trello pane
class SidePaneTrello(TrelloWidget):
	def __init__(self, ui, pageview, plugin):
		TrelloWidget.__init__(self, ui, pageview, plugin)
		self.set_policy(gtk.POLICY_NEVER,gtk.POLICY_AUTOMATIC)
		self.set_shadow_type(gtk.SHADOW_IN)
		self.set_size_request(-1,200)  # Fixed Height

# Dialog for opening a trello card
class OpenTrelloCardDialog(Dialog):
	_treeview=None
	_liststore=None
	def __init__(self, ui, preferences):
		title = _('Open Trello Card')
		Dialog.__init__(self, ui, title)

		# Set layout of Window
		self.set_default_size(600, 400)

		# TODO allow changing preferences in this dialog and rerunning the query
		# get cards
		# TODO spinny?
		cards=self._get_cards(
			preferences["assigned"],
			(preferences["board"],"")[preferences["board"] is None],
			(preferences["list"],"")[preferences["list"] is None]
		)
		self._liststore = self._prepare_liststore(cards)
		self._treeview = self._prepare_treeview_with_headcolumn_list(self._liststore)
		scroll=gtk.ScrolledWindow()
		scroll.set_policy(gtk.POLICY_AUTOMATIC,gtk.POLICY_AUTOMATIC)
		scroll.add(self._treeview)
		self.vbox.pack_start(scroll)

		# TODO doubleclick table row

		self.show_all()

	def _get_cards(self,assigned,board,list):
		# query member for all relevant boards
		boards=TrelloApi.get("members/me/boards")
		boardIndex={}
		for b in boards:
			boardIndex[b["id"]]=b

		# minimize the number of queries we need to make here
		cards=[]
		if assigned and board == "" and list == "":
			cards=TrelloApi.get("members/me/cards")
		else:
			boardIDs=()
			# if board, only select boards matching the board name search
			if board != "":
				boardIDs=(x["shortLink"] for x in boards if board in x["name"])
			# else get all board IDs
			else:
				boardIDs=(x["shortLink"] for x in boards)

			for boardID in boardIDs:
				# if list, query board for lists, filter lists by search, then query lists for cards
				if list != "":
					lists=TrelloApi.get("boards/" + boardID + "/lists")
					listIDs = (x["shortLink"] for x in lists if list in x["name"])
					for listID in listIDs:
						cards+=TrelloApi.get("lists/" + listID + "/cards")
				# else query board for cards directly, no list ID needed
				else:
					cards+=TrelloApi.get("boards/" + boardID + "/cards")

			# finally filter list by assigned
			if assigned:
				# get member ID
				memberID=TrelloApi.get("members/me")[0]["id"]

				cards=[x for x in cards if memberID in x["idMembers"]]

		for card in cards:
			card["board"]=boardIndex[card["idBoard"]]["name"]

		return cards

	def _prepare_liststore(self, data):
		liststore = gtk.ListStore(str,str,str,str)

		for datum in data:
			liststore.append([datum["board"],datum["name"],datum["dateLastActivity"],datum["shortLink"]])

		return liststore

	def _prepare_treeview_with_headcolumn_list(self, liststore):
		treeview = gtk.TreeView(liststore)

		cell = gtk.CellRendererText()
		cell.set_property('editable', False)
		column = gtk.TreeViewColumn(_('Board'), cell, text=0)
		column.set_min_width(80)
		column.set_resizable(True)
		column.set_sort_column_id(0)
		treeview.append_column(column)

		cell = gtk.CellRendererText()
		cell.set_property('editable', False)
		column = gtk.TreeViewColumn(_('Title'), cell, text=1)
		column.set_min_width(80)
		column.set_resizable(True)
		column.set_sort_column_id(1)
		treeview.append_column(column)

		cell = gtk.CellRendererText()
		cell.set_property('editable', False)
		column = gtk.TreeViewColumn(_('Date'), cell, text=2)
		column.set_min_width(80)
		column.set_resizable(True)
		column.set_sort_column_id(2)
		treeview.append_column(column)

		cell = gtk.CellRendererText()
		cell.set_property('editable', False)
		column = gtk.TreeViewColumn(_('ID'), cell, text=3)
		column.set_min_width(80)
		column.set_resizable(True)
		column.set_sort_column_id(3)
		treeview.append_column(column)

		return treeview

	def do_response_ok(self):
		# TODO create page
		if self._treeview.get_selection() is not None:
			selectedelement=self._treeview.get_selection().get_selected()
			if selectedelement is not None:
				self.result = self._liststore.get(self._treeview.get_selection().get_selected()[1],3)[0]
			else:
				self.result=None
			return True

	def do_response_cancel(self):
		self.result = None
		return True